# Демо-проект, проста сімейна бухгалтерія

Даний проект підготовлено для демонстрації у рамках воркшопу.

Для демонстрації використовується Ubuntu 16.04. Необхідно, щоб в системі вже були встановлені наступні компоненти:

* [Python 3.5+](https://www.python.org/)
* [PostgreSQL 9.5+](https://www.postgresql.org/)

Нам також знадобиться спеціалізований текстовий редактор для зручного написання програм, наприклад [Sublime Text 3](https://www.sublimetext.com/).

Код програми на Python - це набір текстових файлів з кодом, тому зручно створити нову папку. І переходимо до неї відповідно:

    $ mkdir familyfinances
    $ cd familyfinances

Створюємо нове віртуальне оточення. Це дуже важливий крок, бо в реальності у вас на комп'ютері, з часом, накопичиться багато різноманітних проектів. Відповідно, обскільки кожен проект використовує певний набір різноманітних бібліотек, часто досить великий, то важливо щоб ці бібліотеки, конкретних версій, були встановлені індивідуально, для кожного такого проекту.

    $ virtualenv -p python3 env_py35

Активуємо віртуальне оточення:

    $ source env_py35/bin/activate

Далі, запустимо текстовий редактор і відкриємо у ньому папку з проектом. Тепер, створимо файл `requirements.txt`. Запишемо у нього перелік бібліотек, що нам знадобляться:

    dj-database-url
    Django
    psycopg2
    waitress
    whitenoise

Ми не вказуємо зараз версії бібліотек, бо будемо використовувати останні доступні. Потім, ми перегенеруємо його вже по факту, щоб мати зафіксовані номери версій. Але, спочатку нам цього не потрібно. Встановити всі ці бібліотеки з аодин раз ми можемо з допомогою наступної команди:

    $ pip install -r requirements.txt

Після того, як бібліотеки встановляться, можемо перегенерувати даний файл так, щоб там вже фігурували бібліотеки разом з конкретними версіями:

    $ pip freeze > requirements.txt

Зверніть увагу, що через баг у видачі буде фігурувати також рядок `pkg-resources==0.0.0`. Необхідно вручну видалити цей рядок, щоб він нам пізніше не заважав. Відповідно, після оновлення, файл міститиме щось таке:

    dj-database-url==0.4.2
    Django==1.11.6
    psycopg2==2.7.3.2
    pytz==2017.2
    waitress==1.1.0
    whitenoise==3.3.1

Наступним кроком, створимо нову базу даних. Для цього зручно використовувати графічний клієнт для `PostgreSQL` - `pgAdmin`. Отже, створюємо нову БД `familyfinances` і рухаємося далі.

Створюємо заготовку проекта:

    $ django-admin startproject familyfinances .

Далі, відкриємо файл `env_py35/bin/activate` і допишемо в кінець файлу наступне (це оголошення системних змінних, це хороший спосіб описувати конфігурацію):

    export DATABASE_URL=postgres://postgres:123456@localhost:5432/familyfinances

Займемося внесенням змін до файлу `settings.py`, щоб навчити його працювати з конфігурацією на базі системних змінних.

    import dj_database_url

    SECRET_KEY = os.environ.get('SECRET_KEY', 'default-secret-key')

    DEBUG = os.environ.get('DEBUG_MODE', 'YES') == 'YES'

    ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '*').split(',')

    DATABASES = {
        'default': dj_database_url.config(default=os.environ['DATABASE_URL']),
    }

    AUTH_PASSWORD_VALIDATORS = []

    LANGUAGE_CODE = 'uk'

    TIME_ZONE = 'Europe/Kiev'

    STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

    ENVIRONMENT = os.environ.get('ENVIRONMENT', 'development')
    if ENVIRONMENT == 'production':
        INSTALLED_APPS = [
            'whitenoise.runserver_nostatic',
        ] + INSTALLED_APPS
        MIDDLEWARE = [MIDDLEWARE[0]] + [
            'whitenoise.middleware.WhiteNoiseMiddleware',
        ] + MIDDLEWARE[1:]
        STATICFILES_STORAGE = \
            'whitenoise.storage.CompressedManifestStaticFilesStorage'

Далі, запускаємо міграції і створюємо суперкористувача:

    $ ./manage.py migrate
    $ ./manage.py createsuperuser

Можемо запустити локальний веб-сервер і перевірити, що в нас є на даний момент:

    $ ./manage.py runserver

Перейменуємо заголовок адмінки, додавши наступний рядок у файл `urls.py`:

    admin.site.site_header = 'Керування сімейними фінансами'

Створимо нову аплікацію в рамках якої будемо описувати роботу з гаманцями:

    $ ./manage.py startapp finances

І додамо в `settings.py` рядок `finances` до опції `INSTALLED_APPS`.

Далі, можемо переходити до файлу `models.py` і описувати структури даних:

    from django.db import models
    from django.utils import timezone


    class Currency(models.Model):
        title = models.CharField('Назва', max_length=70, unique=True)
        format_string = models.CharField(
            'Рядок форматування', max_length=150,
            default='%.2d',
            )

        class Meta:
            verbose_name = 'Валюта'
            verbose_name_plural = 'Валюти'
            ordering = ['title']

        def __str__(self):
            return self.title


    class Wallet(models.Model):
        title = models.CharField('Назва', max_length=70, unique=True)
        currency = models.ForeignKey(
            Currency, verbose_name='Валюта', on_delete=models.CASCADE)
        position = models.PositiveIntegerField('Позиція', default=0)

        class Meta:
            verbose_name = 'Гаманець'
            verbose_name_plural = 'Гаманці'
            ordering = ['position']

        def __str__(self):
            return self.title


    class Transaction(models.Model):
        wallet = models.ForeignKey(
            Wallet, verbose_name='Гаманець',
            related_name='transactions', on_delete=models.CASCADE)
        amount = models.DecimalField(
            'Сума', max_digits=11, decimal_places=2)
        comment = models.TextField('Коментар', blank=True)

        added = models.DateTimeField('Додано', default=timezone.now)

        class Meta:
            verbose_name = 'Транзакція'
            verbose_name_plural = 'Транзакції'
            ordering = ['-added']

        def __str__(self):
            return '%s : %s' % (self.wallet, self.amount)

Генеруємо міграцію і запускаємо її:

    $ ./manage.py makemigrations finances
    $ ./manage.py migrate finances

Додаємо базові налаштування для адмінки у файл `admin.py`:

    from django.contrib import admin
    from .models import (
        Currency,
        Wallet,
        Transaction,
    ) 


    admin.site.register(Currency)
    admin.site.register(Wallet)
    admin.site.register(Transaction)

Власне, цього вже достатньо, щоб мати павний базовий інтерфейс в адмінці. Перейменуємо також блок нашої аплікації в адмінці. Додамо наступну опцію у файл `apps.py`:

    verbose_name = 'Фінанси'

І такий рядок в `__init__.py`:

    default_app_config = 'finances.apps.FinancesConfig'

Вдосконалимо дещо наш інтерфейс адміністратора. Почнемо з інтерфейсу транзакцій:

    class TransactionAdmin(admin.ModelAdmin):
        list_display = ('amount', 'wallet', 'comment', 'added')
        list_filter = ('wallet',)
        date_hierarchy = 'added'
        search_fields = ('comment',)

    admin.site.register(Transaction, TransactionAdmin)

Далі, підправимо інтерфейс валют:

    class CurrencyAdmin(admin.ModelAdmin):
        list_display = ('title', 'format_string')

    admin.site.register(Currency, CurrencyAdmin)

І, в останню чергу, гаманець:

    class WalletAdmin(admin.ModelAdmin):
        list_display = ('title', 'currency', 'position')

    admin.site.register(Wallet, WalletAdmin)

Звичайно, нам хотілося б побачити для кожного гаманця суму. Щоб її порахувати нам необхідно порахувати суму всіх транзакцій. В термінах бази даних це називається агрегацією. Додамо наступні методи до опису моделі `Wallet`:

    def get_amount(self):
        result = self.transactions.aggregate(
            amount=models.Sum('amount'))
        return result['amount'] or 0

    def get_fmt_amount(self):
        return self.currency.format_string % self.get_amount()
    get_fmt_amount.short_description = 'Сума'

Ну і додамо колонку `get_fmt_amount` до `WalletAdmin`.

Наступним кроком зробимо головну сторінку з зведеною інформацією по гаманцям і останнім транзакціям для кожного з них. Додамо нову функцію у `views.py`:

    def dashboard(request):
        from .models import Wallet

        wallets = Wallet.objects.all()
        context = {
            'wallets': wallets,
        }
        return render(request, 'dashboard.html', context)

І створимо файл `urls.py`:

    from django.conf.urls import url
    from .views import dashboard


    urlpatterns = [
        url(r'^$', dashboard),
    ]

Далі, створимо папку `templates` і підготуємо два шаблони. Спочатку `base.html`

    <!doctype html>
    <html lang="en">
      <head>
        <title>Сімейні фінанси</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
      </head>
      <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-between">
          <a class="navbar-brand" href="/">Сімейні фінанси</a>
          <div>
          <ul class="navbar-nav">
            {% if user.is_authenticated %}
            <li class="nav-item" >
                <a class="nav-link" href="{% url 'admin:app_list' 'finances' %}">Керування</a>
            </li>
            <li class="nav-item" >
                <a class="nav-link" href="{% url 'logout' %}">Вийти</a>
            </li>
            {% endif %}
          </ul>
          </div>
        </nav>
        <div class="container">

          {% block content %}{% endblock %}
        </div>
    </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
      </body>
    </html>

І `dashboard.html`:

    {% extends "base.html" %}

    {% block content %}
    <h1 class="mt-4 mb-4">Гаманці</h1>

    <div id="accordion" role="tablist">
        {% for wallet in wallets %}
      <div class="card">
        <div class="card-header" role="tab" id="heading{{ forloop.counter }}">
          <h5 class="mb-0">
            <a data-toggle="collapse" href="#collapse{{ forloop.counter }}" aria-expanded="false" aria-controls="collapse{{ forloop.counter }}">
              {{ wallet }} : {{ wallet.get_fmt_amount }}
            </a>
          </h5>
        </div>

        <div id="collapse{{ forloop.counter }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ forloop.counter }}" data-parent="#accordion">
          <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>Сума</th>
                    <th>Коментар</th>
                    <th>Дата</th>
                </tr>
                {% for transaction in wallet.get_last_transactions %}
                <tr>
                    <td>{{ transaction.amount }}</td>
                    <td>{{ transaction.comment }}</td>
                    <td>{{ transaction.added }}</td>
                </tr>
                {% endfor %}
            </table>
            
          </div>
        </div>
      </div>
        {% endfor %}
    </div>
    {% endblock %}

Ну, і додамо наступний рядок у головний `urls.py`:

    url(r'^', include('finances.urls')),

Щоб вивести суму транзакції правильно відформатованою, з врахуванням валюти, створимо новий шаблонний тег. Для цього, створимо папку `templatetags` і в ній файли `__init__.py` та `finances.py`. В останньому напишемо наступне:

    from django import template


    register = template.Library()


    @register.simple_tag
    def transaction_amount(transaction):
        return transaction.wallet.currency.format_string % transaction.amount


І тепер в шаблоні можемо писати так:

    {% transaction_amount transaction %}

Звичайно є ще один нюанс. Нам потрібно закрити головну сторінку від доступу незареєстрованих користувачів. Тому, додамо ще один рядок у головний `urls.py`:

    url(r'^accounts/', include('django.contrib.auth.urls')),

Допишемо пару рядків у `views.py`:

    from django.contrib.auth.decorators import login_required

    @login_required
    ...

І створимо шаблон `registration/login.html`:

    {% extends "base.html" %}

    {% block content %}

    {% if form.errors %}
    <p>Your username and password didn't match. Please try again.</p>
    {% endif %}

    {% if next %}
        {% if user.is_authenticated %}
        <p>Your account doesn't have access to this page. To proceed,
        please login with an account that has access.</p>
        {% else %}
        <p>Please login to see this page.</p>
        {% endif %}
    {% endif %}

    <form method="post" action="{% url 'login' %}">
    {% csrf_token %}
    <table>
    <tr>
        <td>{{ form.username.label_tag }}</td>
        <td>{{ form.username }}</td>
    </tr>
    <tr>
        <td>{{ form.password.label_tag }}</td>
        <td>{{ form.password }}</td>
    </tr>
    </table>

    <input class="btn btn-primary" type="submit" value="Увійти" />
    <input type="hidden" name="next" value="{{ next }}" />
    </form>
    {% endblock %}

Власне, це вже цілком робочий проект. Єдиний момент - він працює лише на нашому комп'ютері. Скористаємося [Heroku](https://www.heroku.com/pricing) щоб зробити його видимим для світу. Спочатку створимо файл `runtime.txt`:

    python-3.6.2

І файл `Procfile`:

    web: waitress-serve --port=$PORT familyfinances.wsgi:application

Створюємо файл `.gitignore`:

    __pycache__
    env_*
    *.pyc
    /staticfiles

Далі ініціюємо локальний Git репозиторій:

    $ git init
    $ git add -A
    $ git commit -m "initial commit"

Йдемо на [Bitbucket](https://bitbucket.org/) і створюємо там репозиторій. Підключаємо його і синхронізуємо:

    $ git remote add origin git@bitbucket.org:korkholeh/familyfinances.git
    $ git push origin master

Далі, створюємо аплукацію в Heroku. Підключаємо базу даних і прописуємо ще декілька налаштувань. Наприклад, якось так:

* `DATABASE_URL` = `postgres://sjshexdzygqzkz:e5e840a03f1dff6c5a6327856d919d4576fae052882b423ac28cf79ec0f201f5@ec2-54-247-124-9.eu-west-1.compute.amazonaws.com:5432/dbgqarudidv74j`
* `DEBUG_MODE` = `NO`
* `ENVIRONMENT` = `production`
* `SECRET_KEY` = `kjhsad99djoaidq`

Встановимо слієнт Heroku, якщо ще не встановлено:

    $ wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh

Далі робимо наступне:

    $ heroku login
    $ heroku git:remote -a familyfinances
    $ git push origin heroku

Створюємо суперкористувача:

    $ heroku run python manage.py createuser

Для роботи команди `heroku run` необхідно щоб провайдер не закривав доступ до порта 5000. Протестити можна таким чином:

    $ telnet rendezvous.runtime.heroku.com 5000

Якщо є проблеми, то тимчасово вийти з положення можна таким чином:

    $ heroku run:detached python manage.py shell --command="from django.contrib.auth.models import User; User.objects.create_superuser('demouser', 'demo@gmail.com', 'a90123')"

Далі можемо переходити за адресою `https://familyfinances.herokuapp.com/` і експериментувати з проектом на сервері.
