from django.contrib import admin
from .models import (
    Currency,
    Wallet,
    Transaction,
)


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('title', 'format_string')

admin.site.register(Currency, CurrencyAdmin)


class WalletAdmin(admin.ModelAdmin):
    list_display = ('title', 'currency', 'position', 'get_fmt_amount')

admin.site.register(Wallet, WalletAdmin)


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('amount', 'wallet', 'comment', 'added')
    list_filter = ('wallet',)
    date_hierarchy = 'added'
    search_fields = ('comment',)

admin.site.register(Transaction, TransactionAdmin)
