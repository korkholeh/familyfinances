from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Wallet


@login_required
def dashboard(request):
    wallets = Wallet.objects.all()
    context = {
        'wallets': wallets,
    }
    return render(request, 'dashboard.html', context)
