from django import template


register = template.Library()


@register.simple_tag
def transaction_amount(transaction):
    return transaction.wallet.currency.format_string % transaction.amount
