from django.db import models


class Currency(models.Model):
    title = models.CharField('Назва', max_length=70, unique=True)
    format_string = models.CharField(
        'Рядок форматування', max_length=150,
        default='%.2d',
        )

    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюти'
        ordering = ['title']

    def __str__(self):
        return self.title


class Wallet(models.Model):
    title = models.CharField('Назва', max_length=70, unique=True)
    currency = models.ForeignKey(
        Currency, verbose_name='Валюта', on_delete=models.CASCADE)
    position = models.PositiveIntegerField('Позиція', default=0)

    class Meta:
        verbose_name = 'Гаманець'
        verbose_name_plural = 'Гаманці'
        ordering = ['position']

    def __str__(self):
        return self.title

    def get_amount(self):
        result = self.transactions.aggregate(
            amount=models.Sum('amount'))
        return result['amount'] or 0

    def get_fmt_amount(self):
        return self.currency.format_string % self.get_amount()
    get_fmt_amount.short_description = 'Сума'

    def get_last_transactions(self):
        return self.transactions.all()[:10]


class Transaction(models.Model):
    wallet = models.ForeignKey(
        Wallet, verbose_name='Гаманець',
        related_name='transactions', on_delete=models.CASCADE)
    amount = models.DecimalField(
        'Сума', max_digits=11, decimal_places=2)
    comment = models.TextField('Коментар', blank=True)

    added = models.DateTimeField('Додано', auto_now_add=True)
    changed = models.DateTimeField('Змінено', auto_now=True)

    class Meta:
        verbose_name = 'Транзакція'
        verbose_name_plural = 'Транзакції'
        ordering = ['-added']

    def __str__(self):
        return '%s : %s' % (self.wallet, self.amount)
